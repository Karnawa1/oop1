
package oop11;

public class Test1 {

    private int a;
    private int b;

    public Test1(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void display() {
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int sum() {
        return this.a + this.b;
    }

    public int max() {
        return Integer.max(this.a, this.b);
    }
}


