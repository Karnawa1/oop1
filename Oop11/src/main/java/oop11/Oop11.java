/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package oop11;

/**
 *
 * @author spoty
 */
public class Oop11 {

    public static void main(String[] args) {
        var test = new Test1(2, 6);

        test.display();

        test.setA(4);
        test.setB(2);

        System.out.println("sum = " + test.sum());
        System.out.println("max = " + test.max());

        
    }
}
