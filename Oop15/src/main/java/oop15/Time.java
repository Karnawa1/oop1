
package oop15;

public class Time {
    private int hour;
    private int minute;
    private int second;

    // Constructor for setting the time
    public Time(int hour, int minute, int second) {
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }

    // Method for setting the hour with validity check
    public void setHour(int hour) {
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
    }

    // Method for setting the minute with validity check
    public void setMinute(int minute) {
        if (minute >= 0 && minute < 60) {
            this.minute = minute;
        } else {
            this.minute = 0;
        }
    }

    // Method for setting the second with validity check
    public void setSecond(int second) {
        if (second >= 0 && second < 60) {
            this.second = second;
        } else {
            this.second = 0;
        }
    }

    // Method for changing the time by a specified number of hours
    public void addHours(int hours) {
        int newHour = (hour + hours) % 24;
        setHour(newHour);
    }

    // Method for changing the time by a specified number of minutes
    public void addMinutes(int minutes) {
        int newMinute = (minute + minutes) % 60;
        setMinute(newMinute);
    }

    // Method for changing the time by a specified number of seconds
    public void addSeconds(int seconds) {
        int newSecond = (second + seconds) % 60;
        setSecond(newSecond);
    }

    // Method for getting the current time as a string
    public String getTime() {
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }
}
