
package oop15;

public class Oop15 {

    public static void main(String[] args) {
        Time time = new Time(12, 59, 59);
        System.out.println("Initial time: " + time.getTime());
        time.addHours(2);
        System.out.println("Time after adding 2 hours: " + time.getTime());
        time.addMinutes(90);
        System.out.println("Time after adding 90 minutes: " + time.getTime());
        time.addSeconds(3661);
        System.out.println("Time after adding 3661 seconds: " + time.getTime());
    }
}
