
package oop12;

public class Oop12 {

    public static void main(String[] args) {
         var test = new Test2();

        System.out.println("task.getA() = '" + test.getA() + "'");
        System.out.println("task.getB() = '" + test.getB() + "'");
        test.setA("val");
        System.out.println("task.getA() = " + test.getA());
        test.setB("cs");
        System.out.println("task.getB() = " + test.getB());
        var task2 = new Test2("bot", "rak");
        System.out.println("task2.getA() = " + task2.getA());
        System.out.println("task2.getB() = " + task2.getB());

    }
}
