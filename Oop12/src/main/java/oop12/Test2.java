
package oop12;


public final class Test2 {
     private String a;
     private String b;

    public Test2(String a, String b) {
        this.setA(a);
        this.setB(b);
    }

    public Test2() {
        this("", "");
    }

    public String getA() {
        return a;
    }

  
    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

}
