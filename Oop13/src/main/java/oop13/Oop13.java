

package oop13;

public class Oop13 {

    public static void main(String[] args) {
         var triangle = new Triangle(3, 4, 5);

        System.out.println("Area = " + triangle.area());
        System.out.println("Perimeter = " + triangle.perimeter());

    }
}
