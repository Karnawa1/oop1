

package oop14;

public class Oop14 {

    public static void main(String[] args) {
        DecimalCounter counter = new DecimalCounter(16,0,50);
        System.out.println("initial value: " + counter.getValue());
        counter.increase();
        System.out.println("after increase: " + counter.getValue());
        counter.decrease();
        System.out.println("after decrease: " + counter.getValue());
    }
}
