
package oop14;


public class DecimalCounter {
    private int value;
    private final int min;
    private final int max;

    public DecimalCounter() {
        this(0, 0, 100);
    }

    public DecimalCounter(int value, int min, int max) {
        this.value = value;
        this.min = min;
        this.max = max;
    }

    public void increase() {
        if (value < max) {
            value++;
        }
    }

    public void decrease() {
        if (value > min) {
            value--;
        }
    }
    public int getValue() {
        return value;
    }
}
